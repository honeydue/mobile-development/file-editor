//
//  File_EditorApp.swift
//  File Editor
//
//  Created by Lee Faus on 1/5/23.
//

import SwiftUI

@main
struct File_EditorApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
