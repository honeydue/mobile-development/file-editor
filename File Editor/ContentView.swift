//
//  ContentView.swift
//  File Editor
//
//  Created by Lee Faus on 1/5/23.
//

import SwiftUI

struct ContentView: View {
    @State var userText = ""
    @State var capMode = 0
    
    var body: some View {

        VStack{
            Spacer()
            if capMode == 1 {
                Text(userText.uppercased()).font(.largeTitle)
            } else if capMode == 2 {
                Text(userText.lowercased()).font(.largeTitle)
            } else {
                Text(userText.capitalized).font(.largeTitle)
            }
            Spacer()
            Button(action: {
                if capMode == 1 {
                    UIPasteboard.general.string = userText.uppercased()
                } else if capMode == 2 {
                    UIPasteboard.general.string = userText.lowercased()
                } else {
                    UIPasteboard.general.string = userText.capitalized
                }
            }) {
                RoundedButton(title: "Copy", color: .purple)
            }
            HStack {
                Button(action: {
                    capMode = 1
                }) {
                    RoundedButton(title: "CAPS", color: .yellow)
                }
                Button(action: {
                    capMode = 2
                }) {
                    RoundedButton(title: "lower", color: .green)
                }
                Button(action: {
                    capMode = 3
                }) {
                    RoundedButton(title: "Camel", color: .red)
                }
            }
            Spacer()
            TextField("Enter Text...", text: $userText)
        }.padding()
    }
}

struct RoundedButton: View {
    var title: String
    var color: Color = Color.blue
    var body: some View {
        Text(title)
            .padding(10)
            .frame(maxWidth: .infinity)
            .background(color)
            .font(.body)
            .foregroundColor(.white)
            .cornerRadius(10)
            
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
